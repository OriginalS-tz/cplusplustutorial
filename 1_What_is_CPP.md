# What is CPP?

如今的C++不再只是**C with Classes**,而是一个mulitiparadigm programming language(多重范式型编程语言)

支持

- procedural(面向过程)
- object-oriented(面向对象)
- functional(函数形式)
- generic(泛型形式)
- metaprogramming(元编程形式)

那么如何理解这样的一门语言？

答案是:将C++视为语言联邦而不是单一的语言

作为一个语言联邦，C++拥有四种子语言

- C:C是C++的基础,有时候C++的解法不过是较高级的C的解法
- 面对对象的C++:这部分是C with Classes的根本诉求
- Template C++(C++ 模版) : Template的设计已经充斥了C++,实际上由于Template的强大,带来了崭新的programming paradigm(编程范式),也就是Template Metaprogramming(模版元编程),不过,TMP很少与C++主流相互影响
- STL:这是个template程序库,包含containeers, iterators, algorithms, function object

需要记住,每个sublanguage都有自己的规约，在使用不同的子语言的时候，恰当地改变编程策略会让你的程序更好地运行

接下来将简略介绍各个子语言

## C

```cpp
//采用C++风格的头文件形式
#include <iostream>
#include <cstdio>

int main()
{
    std::printf("hello,world");
    std::cout << "hello,world" << std::endl;
    return 0;
}
```

在这部分，写C++和写C几乎没什么区别，区别就是

- 头文件
- namespace(命名空间)

C++风格的头文件不会直接包含`xxx.h`文件,而是包含`xxx`文件，这两者有什么区别呢？区别就在于`namespace`

### namespace

namespace是为了防止命名冲突而引进的一种机制，假设存在两个文件`print_1.h,print_2.h`，里面都包含着同名的`print`，但是实现不同

```cpp
file: print_1.h

#ifndef PRINT_1_H
#define PRINT_1_H
int print()
{
   return 1000; 
}
#endif
-----------------------------
file: print_2.h

#ifndef PRINT_2_H
#define PRINT_2_H
int print()
{
	return 56;
}
#endif
------------------------------
file: main.cpp

#include "print_1.h"
#include "print_2.h"
int main()
{
    int i = print(); // i = 1000;
    return 0;
}
```

这样的话，我们就分不清`main`中的`print`是使用哪个头文件里面的`print`了(我测试的话是最先`include`的那个，也就是`print_1.h`)

如果使用`namespace`，如下

```cpp
file: print_1.h

#ifndef PRINT_1_H
#define PRINT_1_H
namespace P1
{
	int print()
	{
   		return 1000; 
	}
};
#endif
-----------------------------
file: print_2.h

#ifndef PRINT_2_H
#define PRINT_2_H
namespace P2
{
    int print()
	{
		return 56;
	}
};
#endif
----------------------------
file: main.cpp

#include "print_1.h"
#include "print_2.h"
int main()
{
    int i = P1::print(); //use print_1.h's print i = 1000;
    int i2 = P2::print();//use print_2.h's print i2=56
    return 0;
}
```

命名空间的作用非常大，尤其是我们在使用其他人开发的库的时候/或者自己编写库的时候(因为大量使用不同库的时候，函数命名难免有重复)

后期会对其进行详解

## 面向对象的C++

面向对象的C++将使用`class`来组织各种数据，函数`class`的意义在于，为数据和函数创建联系/关联

例如，我们拥有一个学生信息数组，然后需要统计学生的平均分，或者获得某个学生的分数，假设使用C来写

```cpp
struct Student
{
    int score;
    char name[14];
};

Student student_info_list[10];

int getAverageScore()
{
    int all = 0;
    for (int i = 0; i < 10; i++)
    {
        all += student_info_list[i].score;
    }
    return all / 10;
}

int getStudentScore(char *name)
{
    for (int i = 0; i < 10; i++)
    {
        if (strcmp(name, student_info_list[i].name) == 0)
        {
        	return student_info_list[i].score;   
        }
    }
    return -1;
}

int main()
{
    //...do something to initilization the student_info_list array
    int average_score = getAverageScore();
    int score = getStudent("Jam");
    return 0;
}
```

如果仅仅只有两个函数，那么还是很容易懂的

但是如果你这个程序的功能还有(只是简单举例，不代表其合理性)

- 统计职员的平均工资 :  `getAveragePay`
- 获得职员的工资：`getPay`

在程序中调用

```cpp
int main()
{
    int average_score = getAverageScore();
    int average_pay = getAveragePay();
    //more function, which will be named getAveragexxxxx()
    return 0;
}
```

这样看起来就会非常不直观，太多的数据(例如学生信息数组，员工信息数组)也难以组织，如果命名不合理的话，也难以看出数据与函数之间的关系

如果使用面向对象的方法

```cpp
class ScoreManager
{
private:
    Student student_info_list[10];
public:
	int getAverageScore()
	{
    	int all = 0;
    	for (int i = 0; i < 10; i++)
    	{
        	all += student_info_list[i].score;
    	}
    	return all / 10;
	}

	int getStudentScore(char *name)
	{
    	for (int i = 0; i < 10; i++)
    	{
        	if (strcmp(name, student_info_list[i].name) == 0)
        	{
        		return student_info_list[i].score;   
        	}
    	}
    	return -1;
	}
};

int main()
{
    ScoreManager score_manager;
    int average_score = ScoreManger.getAverageScore();
    return 0;
}
```

使用`class`将数据封装起来，然后预留几个接口(`class`中的方法/函数)维持一定限度的访问权限，是非常有利于编程的

我曾经在项目中使用了大量的全局变量(大概有40多个)，这使得后期的编程非常困难，因为随着逻辑的增多，因为所有的数据都属于全局变量，没有使用`class`进行分层，也没有封装进行访问限制，当程序逻辑增加到一定的时候，就会出现

- 不知道这个数据在哪里被访问到了/修改了
- 不知道这个数据是干什么的了(因为都是全局变量，很多功能相似的数据起的名字都差不多)

前车之鉴告诉我们，适度地使用`class`对数据进行封装，然后设计好的接口，将会使得后期的维护变得简单

不过，使用`class`进行封装也要适度，因为`class`反应的是数据与函数之间的关系，如果一个函数和那个`class`没有任何的关系，然后又强行让他们在一起，这就会产生糟糕的接口设计，尤其是在编写一些C++库的时候，良好的接口设计是非常重要的

## C++ Template

模板(template)是为了支持泛型编程(Generic programming)而存在的，所谓泛型，也就是不依赖于具体类型,wiki对其定义如下

> **Generic programming** is a style of [computer programming](https://link.zhihu.com/?target=https%3A//en.wikipedia.org/wiki/Computer_programming) in which [algorithms](https://link.zhihu.com/?target=https%3A//en.wikipedia.org/wiki/Algorithm) are written in terms of [types](https://link.zhihu.com/?target=https%3A//en.wikipedia.org/wiki/Data_type) *to-be-specified-later* that are then *instantiated* when needed for specific types provided as [parameters](https://link.zhihu.com/?target=https%3A//en.wikipedia.org/wiki/Parameter_%28computer_programming%29).
>
> [Generic programming --wikipedia](https://link.zhihu.com/?target=https%3A//en.wikipedia.org/wiki/Generic_programming)



为了更直观的了解，我们先看看相对于一般的编程方式，范型编程是怎么样的

```cpp
#include <iostream>

int max_normal(int a, int b) {
    return a > b? a : b;
}

template <typename T>
T max_generic(T a, T b) {
    return a > b? a : b;
}
#define Log(x) std::cout<<x<<std::endl;
int main() {
    //int
    Log(max_normal(1, 2)); //2
    Log(max_generic(1, 2));//2
    
    //float
    Log(max_normal(1.1, 1.2));//1
    Log(max_generic(1.1, 1.2));//1.2
    return 0;
}
```

对于多种数据类型，普通函数要声明不同的版本

```cpp
float max(float, float);
int max(int, int);
```

这将导致大量的重复工作，为此，我们需要解放自己，这时候泛型编程登场了

```cpp
template<typename T>
T max(T x, T y) {
    return x > y? x : y;
}
```

就一个函数的事，多么简洁而优雅！模板就如蓝图一样，对于实例化模板参数的每一种类型，都从模板中产生一个不同的实体，下面就是测试

```cpp
#include <iostream>
template <typename T>
T max_generic(T a, T b) {
    return a > b? a : b;
}
#define Log(x) std::cout<<x<<std::endl;
int main() {
    //使用 int 进行实例化
    Log(max_generic(1, 2));
    //使用 double 进行实例化
    Log(max_generic(1.1, 1.2));
    return 0;
}
```

然后我们就会得到两个实例...

```cpp
//自动生成的
int max_generic(int a, int b) {
    return a > b? a : b;
}

double max_generic(double a, double b) {
    return a > b? a : b;
}
```

如果还不相信的话，我们就来看看对应的汇编代码

`g++ -g -c source.cpp & objdump -S source.o > source_obj.s`

```cpp
1.o:	file format Mach-O 64-bit x86-64

Disassembly of section __TEXT,__text:
_main:
; int main() {
       0:	55 	pushq	%rbp
       1:	48 89 e5 	movq	%rsp, %rbp
       4:	48 83 ec 10 	subq	$16, %rsp
       8:	bf 01 00 00 00 	movl	$1, %edi
       d:	be 02 00 00 00 	movl	$2, %esi
; max(1, 2);
      12:	e8 00 00 00 00 	callq	0 <_main+0x17>
      17:	f2 0f 10 05 91 00 00 00 	movsd	145(%rip), %xmm0
      1f:	f2 0f 10 0d 91 00 00 00 	movsd	145(%rip), %xmm1
; max(1.1, 1.2);
      27:	89 45 fc 	movl	%eax, -4(%rbp)
      2a:	e8 00 00 00 00 	callq	0 <_main+0x2F>
      2f:	31 c0 	xorl	%eax, %eax
; }
      31:	f2 0f 11 45 f0 	movsd	%xmm0, -16(%rbp)
      36:	48 83 c4 10 	addq	$16, %rsp
      3a:	5d 	popq	%rbp
      3b:	c3 	retq
      3c:	0f 1f 40 00 	nopl	(%rax)

__Z3maxIiET_S0_S0_:
; T max(T x, T y) {
      40:	55 	pushq	%rbp
      41:	48 89 e5 	movq	%rsp, %rbp
      44:	89 7d fc 	movl	%edi, -4(%rbp)
      47:	89 75 f8 	movl	%esi, -8(%rbp)
; return x > y? x : y;
      4a:	8b 75 fc 	movl	-4(%rbp), %esi
      4d:	3b 75 f8 	cmpl	-8(%rbp), %esi
      50:	0f 8e 0b 00 00 00 	jle	11 <__Z3maxIiET_S0_S0_+0x21>
      56:	8b 45 fc 	movl	-4(%rbp), %eax
      59:	89 45 f4 	movl	%eax, -12(%rbp)
      5c:	e9 06 00 00 00 	jmp	6 <__Z3maxIiET_S0_S0_+0x27>
      61:	8b 45 f8 	movl	-8(%rbp), %eax
      64:	89 45 f4 	movl	%eax, -12(%rbp)
      67:	8b 45 f4 	movl	-12(%rbp), %eax
      6a:	5d 	popq	%rbp
      6b:	c3 	retq
      6c:	0f 1f 40 00 	nopl	(%rax)

__Z3maxIdET_S0_S0_:
; T max(T x, T y) {
      70:	55 	pushq	%rbp
      71:	48 89 e5 	movq	%rsp, %rbp
      74:	f2 0f 11 45 f8 	movsd	%xmm0, -8(%rbp)
      79:	f2 0f 11 4d f0 	movsd	%xmm1, -16(%rbp)
; return x > y? x : y;
      7e:	f2 0f 10 45 f8 	movsd	-8(%rbp), %xmm0
      83:	66 0f 2e 45 f0 	ucomisd	-16(%rbp), %xmm0
      88:	0f 86 0f 00 00 00 	jbe	15 <__Z3maxIdET_S0_S0_+0x2D>
      8e:	f2 0f 10 45 f8 	movsd	-8(%rbp), %xmm0
      93:	f2 0f 11 45 e8 	movsd	%xmm0, -24(%rbp)
      98:	e9 0a 00 00 00 	jmp	10 <__Z3maxIdET_S0_S0_+0x37>
      9d:	f2 0f 10 45 f0 	movsd	-16(%rbp), %xmm0
      a2:	f2 0f 11 45 e8 	movsd	%xmm0, -24(%rbp)
      a7:	f2 0f 10 45 e8 	movsd	-24(%rbp), %xmm0
      ac:	5d 	popq	%rbp
      ad:	c3 	retq
```

我们可以看到max一共有两个函数名

- __Z3maxIiET_S0_S0 = `int max(int, int)`
- __Z3maxIdET_S0_S0,=`double max(double, double)`

以上就是模版的简单作用和原理，下面看看大量使用模版的STL

## C++ STL

C++ STL(Standard Template Libaray)是C++中非常强大的标准库，里面有大量线程的数据结构(例如队列，链表，动态数组)以及常用的算法(搜索，排序，高阶函数)以及其他的工具(函数对象，多线程，正则)

用好STL是进阶的第一步

例如下面的动态数组`vector`，我们再也不必为数组空间担忧

```cpp
#include <vector>
#include <iostream>
int main()
{
    std::vector<int> iV;
    //将0-99存入vector中
    for (int i = 0; i < 100; i++)
    {
        iV.push_back(i);
    }
    //遍历vector
    for (int i : iV)
    {
        std::cout << i << std::endl;
    }
    return 0;
}
```

## 刚刚开始的C++ Tutorial

C++是非常强大的一门语言(最近刚刚击败了python重回世界排名第三)

通过上面简单的介绍，相信会对C++有一个整体的了解，那接下来，就让我们开始学习吧……(未完待续)